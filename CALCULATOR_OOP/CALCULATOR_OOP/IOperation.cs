﻿using CALCULATOR_OOP.Model;

namespace CALCULATOR_OOP
{
    public interface IOperation
    {
        Operation Calculate();
    }
}