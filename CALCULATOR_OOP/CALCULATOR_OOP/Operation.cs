﻿namespace CALCULATOR_OOP
{
    public abstract class Operation
    {
        public object FirstArgument { get; set; }
        public object SecondArgument { get; set; }
        public object Result { get; set; }
    }
}